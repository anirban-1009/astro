import javax.swing.*;
import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.*;


public class Trail1 implements ItemListener
{
    JRadioButton r1, r2, r3;
    CheckboxGroup lngGrp;
    JButton b1, b2, b3, b4;
    JFrame f;
    JPanel panel, pan1;
    JLabel pic;


    public Trail1() throws IOException
    {
    
        f = new JFrame("Add an Image to a JPanel");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLayout(new BorderLayout());
        panel = new JPanel(new FlowLayout());
        pan1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        
        panel.setBounds(50, 50, 250, 250);

        BufferedImage img = ImageIO.read(new File("CodeSpeedy.png"));
        pic = new JLabel(new ImageIcon(img));
        panel.add(pic);

        f.add(panel, BorderLayout.CENTER);

        
        b1 = new JButton("Hello");
        b1.setBackground(new Color(163,23,156,255));
        b1.addItemListener(this);
        pan1.add(b1);
        b2 = new JButton("b2");
        b2.setBackground(new Color(163,23,156,255));
        b2.addItemListener(this);
        pan1.add(b2);
        b3 = new JButton("b3");
        b3.setBackground(new Color(163,23,156,255));
        b3.addItemListener(this);
        pan1.add(b3);
        b4 = new JButton("b4");
        b4.setBackground(new Color(163,23,156,255));
        b4.addItemListener(this);
        pan1.add(b4);
        pan1.setLayout( new GridLayout(2,2));
        f.add(pan1, BorderLayout.SOUTH);


        f.setSize(400, 450);
        f.setVisible(true);
            
        
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if(e.getSource() == "b1"){
            System.out.println("b1");
        }
        else if(e.getSource() == b2){
            System.out.println("b2");
        }
        else if(e.getSource() == b3){
            System.out.println("b3");
        }
        else if(e.getSource() == b4){
            System.out.println("b4");
        }
    }
    public static void main(String args[]) throws IOException 
    {
        Trail1 r = new Trail1();
    }

}