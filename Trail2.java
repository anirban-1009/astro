import javax.swing.*;
import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.*;


public class Trail2 implements ItemListener
{
    JRadioButton r1, r2, r3;
    CheckboxGroup lngGrp;
    JFrame f;
    JPanel panel, pan1;
    JLabel pic;
    JCheckBox chbx1, chbx2, chbx3, chbx4;


    public Trail2() throws IOException
    {
    
        f = new JFrame("Add an Image to a JPanel");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLayout(new BorderLayout());
        panel = new JPanel(new FlowLayout());
        pan1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        
        panel.setBounds(50, 50, 250, 250);

        BufferedImage img = ImageIO.read(new File("CodeSpeedy.png"));
        pic = new JLabel(new ImageIcon(img));
        panel.add(pic);

        f.add(panel, BorderLayout.CENTER);

        
        chbx1 = new JCheckBox("Hello");
        chbx1.setBackground(new Color(163,23,156,255));
        chbx1.addItemListener(this);
        pan1.add(chbx1);


        chbx2 = new JCheckBox("b2");
        chbx2.setBackground(new Color(163,23,156,255));
        chbx2.addItemListener(this);
        pan1.add(chbx2);


        chbx3 = new JCheckBox("b3");
        chbx3.setBackground(new Color(163,23,156,255));
        chbx3.addItemListener(this);
        pan1.add(chbx3);

        chbx4 = new JCheckBox("b4");
        chbx4.setBackground(new Color(163,23,156,255));
        chbx4.addItemListener(this);
        pan1.add(chbx4);


        pan1.setLayout(new FlowLayout());
        f.add(pan1, BorderLayout.SOUTH);


        f.setSize(400, 450);
        f.setVisible(true);
            
        
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if(e.getSource() == chbx1){
            System.out.println("b1");
        }
        else if(e.getSource() == chbx2){
            System.out.println("b2");
        }
        else if(e.getSource() == chbx3){
            System.out.println("b3");
        }
        else if(e.getSource() == chbx4){
            System.out.println("b4");
        }
    }
    public static void main(String args[]) throws IOException 
    {
        Trail2 r = new Trail2();
    }

}