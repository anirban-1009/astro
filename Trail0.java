    import javax.swing.*;
    import java.io.*;
    import java.awt.image.BufferedImage;
    import javax.imageio.ImageIO;
    import java.awt.*;


    public class Trail0
    {
        public JRadioButton r1, r2, r3;
        CheckboxGroup lngGrp;
        public JButton b1, b2, b3, b4;


        public Trail0()
        {
            try
            {
                JFrame f = new JFrame("Add an Image to a JPanel");
                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                f.setLayout(new BorderLayout());
                JPanel panel = new JPanel(new FlowLayout());
                JPanel pan1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
                
                panel.setBounds(50, 50, 250, 250);

                BufferedImage img = ImageIO.read(new File("CodeSpeedy.png"));
                JLabel pic = new JLabel(new ImageIcon(img));
                panel.add(pic);

                f.add(panel, BorderLayout.CENTER);

                
                b1 = new JButton("Hello");
                b1.setBackground(new Color(163,23,156,255));
                pan1.add(b1);
                b2 = new JButton("b2");
                b2.setBackground(new Color(163,23,156,255));
                pan1.add(b2);
                b3 = new JButton("b3");
                b3.setBackground(new Color(163,23,156,255));
                pan1.add(b3);
                b4 = new JButton("b4");
                b4.setBackground(new Color(163,23,156,255));
                pan1.add(b4);
                pan1.setLayout( new GridLayout(2,2));
                f.add(pan1, BorderLayout.SOUTH);


                f.setSize(400, 450);
                f.setVisible(true);
                
            }
            catch(IOException e){}
        }
        public static void main(String args[]) throws IOException 
        {
            Trail0 r = new Trail0();
        }
    }