package Support;
import javax.swing.*;
import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.awt.*;


public class Applet0
{
    public JRadioButton r1, r2, r3;
    CheckboxGroup lngGrp;

    public Applet0()
    {
        try
        {
            JFrame f = new JFrame("Add an Image to a JPanel");
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            f.setLayout(new GridLayout(3,1));
            JPanel panel = new JPanel(new FlowLayout());
            JPanel pan1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
            
            panel.setBounds(50, 50, 250, 250);

            BufferedImage img = ImageIO.read(new File("CodeSpeedy.png"));
            JLabel pic = new JLabel(new ImageIcon(img));
            panel.add(pic);

            f.add(panel);


            JLabel l2 = new JLabel("Buttons");
            pan1.add(l2);
            lngGrp = new CheckboxGroup();
            Checkbox r1 = new Checkbox("Red", lngGrp, true);
            pan1.add(r1);
            Checkbox r2 = new Checkbox("Red", lngGrp, true);
            pan1.add(r2);
            JButton b1 = new JButton("Submit");
            pan1.add(b1);

            pan1.setLayout(new FlowLayout());
            f.add(pan1);


            f.setSize(400, 600);
            f.setVisible(true);
            
        }
        catch(IOException e){}
    }
    public static void main(String args[]) throws IOException 
    {
        Applet0 r = new Applet0();
    }
}