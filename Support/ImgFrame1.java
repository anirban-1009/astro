package Support;
import javax.swing.*;
import java.awt.*;

class ImageFrame1 extends JFrame{
    public JPanel p, p1;
    public JRadioButton op1, op2, op3;
    public JLabel label;

    public void ImgFrame1(){
        JFrame frame = new JFrame("Picture");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridLayout(2, 1));
        frame.setSize(800, 400);

        p = new JPanel(new FlowLayout());
        p1 = new JPanel(new FlowLayout());

        Container c = p.getRootPane();

        label = new JLabel();
        label.setIcon(new ImageIcon("CodeSpeedy.png"));
        Dimension size = label.getPreferredSize();
        label.setBounds(50, 30, size.width, size.height);

        c.add(label);
        frame.add(c);

        op1 = new JRadioButton("Add1");
        op2 = new JRadioButton("Add2");
        op3 = new JRadioButton("Add3");

        p1.add(op1);
        p1.add(op2);
        p1.add(op3);
        frame.add(p1);

        frame.setVisible(true);

    }

    public static void main(String args[]){
        ImageFrame1 a= new ImageFrame1();
    }
}