package Support;
import java.io.*;

import javax.imageio.ImageIO;
import javax.swing.*;

public class BackGimg {

    public static void main(String[] args) {
        JFrame f = new JFrame();
        try {
            f.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("CodeSpeedy.png")))));
        } catch (IOException e) {
            e.printStackTrace();
        }
        f.pack();
        f.setVisible(true);
    }

}