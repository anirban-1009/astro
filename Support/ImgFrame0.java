package Support;
//Java Program to Add Image in Jframe 
import javax.swing.*;
import java.awt.*;

public class ImgFrame0 extends JFrame {
    JPanel p1, p2;
    public static void main(String[] args) {

        JRadioButton op1,op2,op3;
        op1 = new JRadioButton("Add1");
        op2 = new JRadioButton("Add2");
        op3 = new JRadioButton("Add3");

        JFrame frame = new JFrame(); //JFrame Creation       
        frame.setTitle("Add Image"); //Add the title to frame
        frame.setLayout(new BorderLayout(10,10)); //Terminates default flow layout
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Terminate program on close button
        frame.setBounds(100, 200, 350, 300); //Sets the position of the frame
        
        Container c = frame.getContentPane(); //Gets the content layer

        JLabel label = new JLabel(); //JLabel Creation
        label.setIcon(new ImageIcon("CodeSpeedy.png")); //Sets the image to be displayed as an icon
        Dimension size = label.getPreferredSize(); //Gets the size of the image
        label.setBounds(50, 30, size.width, size.height); //Sets the location of the image
        
        c.add(label,BorderLayout.EAST); //Adds objects to the container
        frame.add(op1,BorderLayout.CENTER);
        frame.add(op2,BorderLayout.CENTER);
        frame.add(op3,BorderLayout.CENTER);


        frame.setVisible(true); // Exhibit the frame

    }
}