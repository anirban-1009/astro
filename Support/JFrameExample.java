package Support;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.*;

class JFrameExample extends JFrame{
    JButton b1;
    JFrameExample()
    {
        b1 = new JButton("Hello");
        add(b1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout());
        setSize(300, 300);
        setVisible(true);
    }

    public static void main(String args[]){
        new JFrameExample();
    }
}