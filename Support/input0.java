package Support;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
class input0 extends JFrame implements ActionListener{
    JButton b1;
    JTextField t1,t2,t3;
    JLabel l1, l2;
    input0()
    {
        b1 = new JButton("calc");
        l1 = new JLabel("First NUM:");
        l2 = new JLabel("second NUM:");
        t1 = new JTextField(20);
        t2 = new JTextField(20);
        t3 = new JTextField(20);

        add(l1);
        add(t1);
        add(l2);
        add(t2);
        add(t3);
        add(b1);
        b1.addActionListener(this);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout(FlowLayout.RIGHT,20,25));
        setSize(400, 400);
        setVisible(true);
    }
    public void actionPerformed(ActionEvent a1)
    {
        try{
            int a = Integer.parseInt(t1.getText());
            int b = Integer.parseInt(t2.getText());
            
            double c = a/b;
            t3.setText("" + c);
        }
        catch(NumberFormatException w){
            JOptionPane.showMessageDialog(null, "U have entered text");
        }

        catch(ArithmeticException w){
            JOptionPane.showMessageDialog(null, "U are trying to divide by zero");
        }
    }

    public static void main(String args[]){
        new input0();
    }
}